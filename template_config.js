// Configuration template file.
// NOTE: No not fill out the {}

var config = {};

config.mongodb = {};
config.image = {};

config.mongodb.ip = '';
config.mongodb.name = '';

config.mongodb.username = '';
config.mongodb.password = '';

config.image.folder = ''

config.image.frontpagelimit = 30;

config.image.topviews = 0;

module.exports = config;
